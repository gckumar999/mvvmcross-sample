﻿using System;
using mvvmcross.sample.shared.ViewModel;
using MvvmCross.iOS.Support.XamarinSidebar;
using MvvmCross.iOS.Views;
using UIKit;

namespace mvvmcross.sample.iOS.Views
{
    [MvxSidebarPresentation(MvxPanelEnum.Left, MvxPanelHintType.ResetRoot, true)]
    public partial class HomeView : MvxViewController<HomeViewModel>
    {
        public HomeView() : base("HomeView", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

