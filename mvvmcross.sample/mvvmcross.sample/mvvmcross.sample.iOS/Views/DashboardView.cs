﻿using System;
using mvvmcross.sample.shared.ViewModel;
using MvvmCross;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Support.XamarinSidebar;
using MvvmCross.iOS.Views;
using UIKit;

namespace mvvmcross.sample.iOS.Views
{
    [MvxSidebarPresentation(MvxPanelEnum.Center, MvxPanelHintType.ResetRoot, true)]
    public partial class DashboardView : MvxViewController<DashboardViewModel>
    {

        public DashboardView(IntPtr handle) : base(handle)
        {
        }
        public DashboardView() : base("DashboardView", null)
        {
        }

        public override void ViewDidLoad()
        {
            try
            {
                base.ViewDidLoad();
                //this.View.BackgroundColor = UIColor.Red;
                NavigationController.SetNavigationBarHidden(true, true);
                var masterButton = new UIButton();
                masterButton.TitleLabel.Text = "chaitanya";
                masterButton.SetTitle("Show Master View", UIControlState.Normal);
                var bindingSet = this.CreateBindingSet<DashboardView, DashboardViewModel>();
                bindingSet.Bind(btnNavigate).For("TouchDown").To(vm => vm.ShowMasterCommand);
                bindingSet.Apply();
                Add(masterButton);
            }
            catch(Exception e)
            {

            }
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

