﻿using System;
using System.ComponentModel;
using mvvmcross.sample.core.Models;

namespace mvvmcross.sample.shared.Models
{
    public class UserContext : INotifyPropertyChanged
    {
        public User User { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
