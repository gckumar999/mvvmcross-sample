﻿using System;
using mvvmcross.sample.shared.Interface;
using mvvmcross.sample.shared.Models;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.Platform;

namespace mvvmcross.sample.shared.ViewModel
{
    public class DashboardViewModel : BaseViewModel
    {
        public string Name = "DashboardViewModel";
        private readonly IMvxNavigationService navigationService;
        private UserContext Session = Mvx.Resolve<UserContext>();
        public IMvxCommand ShowMasterCommand
        {
            get
            {
                return new MvxCommand(ShowMasterCommandExecuted);
            }
        }
        private void ShowMasterCommandExecuted()
        {
            var shareInterface = Mvx.Resolve<ISharedService>();
            var coreInterface = Mvx.Resolve<ICoreService>();
            Session.User = new core.Models.User();
            Session.User.Username = "Chaitanya";
            ShowViewModel<HomeViewModel>();
        }

        public DashboardViewModel()
        {
            //_navigationService = navigationService ?? throw new ArgumentNullException(nameof(navigationService));
        }
    }
}
