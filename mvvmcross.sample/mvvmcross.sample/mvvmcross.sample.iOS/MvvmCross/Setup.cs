﻿using System;
using System.Threading;
using System.Threading.Tasks;
using mvvmcross.sample.iOS.core.Service;
using mvvmcross.sample.shared.Interface;
using mvvmcross.sample.shared.Interface.Implementation;
using mvvmcross.sample.shared.Models;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.IoC;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Support.XamarinSidebar;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using UIKit;

namespace mvvmcross.sample.iOS.MvvmCross
{
    public class Setup : MvxIosSetup
    {
        public Setup(IMvxApplicationDelegate applicationDelegate, UIWindow window)
           : base(applicationDelegate, window)
        {

        }

        protected override void InitializeFirstChance()
        {
            MvxIoCProvider.Initialize();
            Mvx.ConstructAndRegisterSingleton<UserContext, UserContext>();
            Mvx.RegisterType<ISharedService, SharedService>();
            Mvx.RegisterType<ICoreService, CoreService>();
            base.InitializeFirstChance();
        }

        protected override IMvxApplication CreateApp()
        {
            return new MvxApp();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override IMvxIosViewPresenter CreatePresenter()
        {
            return new MvxSidebarPresenter((MvxApplicationDelegate)ApplicationDelegate, Window);
        }
    }
    
}
