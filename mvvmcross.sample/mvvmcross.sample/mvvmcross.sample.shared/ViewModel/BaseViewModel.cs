using System;
using MvvmCross.Core.ViewModels;

namespace mvvmcross.sample.shared.ViewModel
{
    public class BaseViewModel : MvxViewModel
    {
        public string ExampleValue { get; set; }

        public IMvxCommandCollection Commands { get; protected set; }

        public virtual void OnLoad()
        {
        }

        public virtual void Reinitialize()
        { }

        public string PageId { get; set; } = Guid.NewGuid().ToString();
        public string Title { get; set; }

        public BaseViewModel()
        {
            Commands = new MvxCommandCollectionBuilder().BuildCollectionFor(this);
        }

        public virtual void CloseCommand()
        {
            Close(this);
        }

        public override void RaiseAllPropertiesChanged()
        {
            try
            {
                base.RaiseAllPropertiesChanged();
            }
            catch (OperationCanceledException)
            {
                //gulp
            }
        }


    }
}