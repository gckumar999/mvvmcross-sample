﻿using System;
using mvvmcross.sample.iOS.core.Service;
using MvvmCross.Core.ViewModels;
using MvvmCross.IoC;
using MvvmCross.Platform;

namespace mvvmcross.sample.iOS.MvvmCross
{
    public class MvxApp : MvxApplication
    {
        public override void Initialize()
        {
            //CreatableTypes().EndingWith("Service").AsInterfaces().RegisterAsLazySingleton();
            typeof(CoreService).Assembly.CreatableTypes().EndingWith("Service").AsInterfaces().RegisterAsLazySingleton();
            Mvx.ConstructAndRegisterSingleton<IMvxAppStart, AppStart>();
            var appStart = Mvx.Resolve<IMvxAppStart>();  
            RegisterAppStart(appStart);
        }

        protected override IMvxViewModelLocator CreateDefaultViewModelLocator()
        {
            return new MyViewModelLocator();
        }

        
    }
}
