﻿using System;
using System.Collections.Generic;
using mvvmcross.sample.shared.ViewModel;
using MvvmCross.Core.ViewModels;

namespace mvvmcross.sample.iOS.MvvmCross
{
    public class MyViewModelLocator
  : MvxDefaultViewModelLocator
    {

        
        public override IMvxViewModel<TParameter> Load<TParameter>(Type viewModelType, TParameter param, IMvxBundle parameterValues, IMvxBundle savedState)
        {
            return base.Load(viewModelType, param, parameterValues, savedState);
        }

        public override IMvxViewModel Load(Type viewModelType, IMvxBundle parameterValues, IMvxBundle savedState)
        {
            return base.Load(viewModelType, parameterValues, savedState);
        }

        public override IMvxViewModel<TParameter> Reload<TParameter>(IMvxViewModel<TParameter> viewModel, TParameter param, IMvxBundle parameterValues, IMvxBundle savedState)
        {
            return base.Reload(viewModel, param, parameterValues, savedState);
        }

        public override IMvxViewModel Reload(IMvxViewModel viewModel, IMvxBundle parameterValues, IMvxBundle savedState)
        {
            return base.Reload(viewModel, parameterValues, savedState);
        }


    }
}
