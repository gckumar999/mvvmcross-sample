﻿using System;
namespace mvvmcross.sample.shared.Interface
{
    public interface ISharedService
    {
        string Name { get; set; }
    }
}
