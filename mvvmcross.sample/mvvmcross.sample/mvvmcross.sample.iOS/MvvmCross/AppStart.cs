﻿using System;
using System.Threading.Tasks;
using mvvmcross.sample.shared.ViewModel;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;

namespace mvvmcross.sample.iOS.MvvmCross
{
    public class AppStart : MvxNavigatingObject, IMvxAppStart
    {
        public void Start(object hint = null)
        {
            //ShowViewModel<CenterPanelViewModel>();
            ShowViewModel<DashboardViewModel>();
        }
    }
}
