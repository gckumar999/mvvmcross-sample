﻿using System;
namespace mvvmcross.sample.core.Models
{
    public class User
    {
        public string Username { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Email { get; set; }

        public string Language { get; set; }
    }
}
