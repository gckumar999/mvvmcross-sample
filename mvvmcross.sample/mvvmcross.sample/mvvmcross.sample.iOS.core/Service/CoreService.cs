﻿using System;
using mvvmcross.sample.shared.Interface;

namespace mvvmcross.sample.iOS.core.Service
{
    public class CoreService :ICoreService
    {
        public string _name = "Shared Interface";


        public string Name

        {

            get { return _name; }

            set { _name = value; }

        }
    }
}
