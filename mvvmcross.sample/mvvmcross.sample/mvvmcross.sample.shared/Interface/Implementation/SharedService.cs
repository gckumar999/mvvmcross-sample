﻿using System;
namespace mvvmcross.sample.shared.Interface.Implementation
{
    public class SharedService : ISharedService
    {

        private string _name = "Shared Interface";
      

        public string Name

        {

            get { return _name; }

            set { _name = value; }

        }
    }
}
